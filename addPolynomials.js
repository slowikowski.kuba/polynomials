const firstPoly = { 
	0: -1,
	2: 2,
    3: 400
}

const secondPoly = {
    0.1: 2,
    1.2: 5,
    2: -3,
    4.5: 6
}

export const addPolynomials = (first, second) => {
    const tmpResult = {...first}

  
    for( let key in second ) {
  	    tmpResult[key] ? tmpResult[key] += second[key] : tmpResult[key] = second[key]
    }
  
    return Object.fromEntries(Object.entries(tmpResult).filter(([_, value]) => value !== 0)) 
}

console.log(addPolynomials(firstPoly, secondPoly))
