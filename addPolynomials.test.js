import { addPolynomials } from './addPolynomials'
describe('addPolynomials', () => {
    const positivePoly = {
        '1': 1,
    }

    const negativePoly = {
        '1': -1,
    }

    const threeExponentsPoly = {
        '1': 2,
        '2.5': 6,
        '4': 8,
    }
    const fourExponentsPoly = {
        '2': 5,
        '2.5': -9,
        '4': -18,
        '7': 9,
    }

    const polySum = {
        '1': 2,
        '2': 5,
        '2.5': -3,
        '4': -10,
        '7': 9,
    }
    it('returns empty object when both input polynomials are empty', () => {
        expect(addPolynomials({}, {})).toEqual({})
    })

    it('returns first polynomial if the second input is an empty object', () => {
        expect(addPolynomials(positivePoly, {})).toEqual(positivePoly)
    })

    it('returns second polynomial if the first input is an empty object', () => {
        expect(addPolynomials({}, positivePoly)).toEqual(positivePoly)
    })

    it('adds coefficients', () => {
        expect(addPolynomials(threeExponentsPoly, fourExponentsPoly)).toEqual(polySum)
    })
    
    it('does not leave exponents in object when the sum of coefficients equals zero', () => {
        expect(addPolynomials(positivePoly, negativePoly)).toEqual({})
    })
    

})